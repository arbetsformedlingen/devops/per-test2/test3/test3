FROM docker.io/python:3.12.7-slim-bookworm AS base

WORKDIR /app


COPY run.py requirements.txt /app/

RUN cd /app \
    && python -m venv venv \
    && . venv/bin/activate \
    && pip install -r requirements.txt


CMD . venv/bin/activate && python run.py
